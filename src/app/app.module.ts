import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {MatButtonModule, MatCardModule, MatFormFieldModule, MatIconModule, MatInputModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TextAreaComponent} from "./text-area/text-area.component";
import {JsonHandlerService} from "./service/jsonHandler.service";
import {OverlayModule} from '@angular/cdk/overlay';
import {RenderFormsComponent} from "./dynamicForms/render-forms.component";



@NgModule({
    declarations: [
        AppComponent,
        TextAreaComponent,
        RenderFormsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        OverlayModule
    ],
    providers: [JsonHandlerService],
    bootstrap: [AppComponent],
    exports: [TextAreaComponent,RenderFormsComponent]
})
export class AppModule {
}
