import {Component, ElementRef, Input, OnChanges, OnInit, Renderer2, ViewChild} from '@angular/core';
import {JsonHandlerService} from "../service/jsonHandler.service";
import {Model} from "../models/formField.model";
import FormModel=Model.FormModel;

@Component({
    selector: 'render-forms',
    templateUrl: './render-forms.component.html',
    styleUrls: ['./render-forms.component.scss']
})
export class RenderFormsComponent implements OnChanges, OnInit {
    @ViewChild('dynamicForms')
    private dynamicForm: ElementRef<ElementRef>;
    @Input()
    formInputs;
    formFields: FormModel;

    constructor(private jsonHandler: JsonHandlerService,
                private renderer: Renderer2,
                private el: ElementRef) {
    }

    ngOnChanges() {
        this.formInputs.subscribe(formFields => {
            this.formFields = formFields;
            if (this.formFields) {
                let formNode=document.getElementsByTagName("FORM");
                if (formNode.length>0){
                 this.renderer.removeChild(this.dynamicForm.nativeElement, formNode[0])

                }
                const form = this.renderer.createElement(this.formFields.form.tag);
                this.renderer.appendChild(this.dynamicForm.nativeElement, form);
                this.formFields.formFieldModel.forEach(item => {
                    const div = this.renderer.createElement('div');
                    const input = this.renderer.createElement(item.tag);
                    this.renderer.appendChild(div, input);
                    this.renderer.appendChild(form, div);
                    if (item.label) {
                        const label = this.renderer.createElement('label');
                        this.renderer.setAttribute(this.el.nativeElement, 'for', item.id);
                        const text = this.renderer.createText(item.label.name);
                        this.renderer.appendChild(label, text);
                        this.renderer.appendChild(div, label);
                        if (item.type == 'text' || item.tag =='select') {
                            this.renderer.insertBefore(div, label, input);

                        }
                    }
                    if (item.name) {
                        this.renderer.setAttribute(input, 'name', item.name);
                    }

                    this.renderer.setAttribute(input, 'id', item.id);
                    if (item.value) {
                        this.renderer.setAttribute(input, 'value', item.value);

                    }
                    if (item.validator) {
                        item.validator.forEach((validator) => {
                            this.renderer.setAttribute(input, validator.key, validator.value);
                        })
                    }
                    if (item.action) {
                        this.renderer.setAttribute(input, 'action', item.action);

                    }

                    if (item.type) {
                        this.renderer.setAttribute(input, 'type', item.type);

                    }
                    if (item.placeholder) {
                        this.renderer.setAttribute(input, 'placeholder', item.placeholder);

                    }
                    if (item.options && item.tag == 'select') {
                        if (item.options) {
                            item.options.forEach((options) => {
                                const optionTag = this.renderer.createElement('option');
                                this.renderer.setAttribute(optionTag, options.key, options.value);
                                const optionsText = this.renderer.createText(options.value);
                                this.renderer.appendChild(input, optionTag);
                                this.renderer.appendChild(optionTag, optionsText);
                            })
                        }
                    }

                })
            } else {
                this.renderer.destroy();
            }

        });
    }

    ngOnInit() {
    }
}
