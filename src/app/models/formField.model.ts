export namespace Model {
    export interface FormModel {
        form: Form;
        formFieldModel: FormFieldModel[];
    }

    export interface FormFieldModel {
        label: Label;
        tag: string;
        name: string;
        id: string;
        value: string;
        validator: Validator[];
        class: any[];
        action: string;
        options: Options[];
        placeholder: string;
        type: string;

    }


    export interface Options {
        key: string;
        value: string;
    }

    export interface Validator {
        key: string;
        value: string;
        tag: string
    }

    export interface Label {
        name: string;

    }
    export interface Form {
        tag: string;
        name: string;

    }

}