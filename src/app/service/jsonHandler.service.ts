import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs/index";
import {Model} from "../models/formField.model";
import FormModel=Model.FormModel;

@Injectable({
    providedIn: 'root'
})
export class JsonHandlerService {
    formField: FormModel;

    handleFormFields(jsonInput):Observable<FormModel> {
        if (JSON.parse(jsonInput)) {
            this.formField  = JSON.parse(jsonInput);
        }
        return of( this.formField)
    }
}